#!/usr/bin/perl

use strict;
use LWP::UserAgent;
use Getopt::Long qw (:config gnu_getopt);
use Pod::Usage;

# add --help option and show help if script invoked
# without parameters
my $opt_help = 0;
my $opt_issue = 0;
GetOptions('help|?' => \$opt_help) or pod2usage(1);
pod2usage(1) if $opt_help;
pod2usage("$0: No issue given. Try `$0 --help' for more information.\n") if ((@ARGV == 0) && (-t STDIN));

# Parameters
my $url ="https://salsa.debian.org/debian-keyring/keyring/raw/master/debian/changelog";

my $ua = LWP::UserAgent->new;
my $req = HTTP::Request->new(GET => $url);
$ua->timeout("10");
my $res = $ua->request($req);

## Check the outcome of the response
$res->is_success or die 'Input file cannot be fetched';
my $content= $res->content;

my $date ;

my $previous_dpn = $ARGV[0] ;

foreach my $line (split "\n",$content) {

    if ($line =~ /debian-keyring \((.*)\) unstable/ ) {
        $date = $1 ;
        $date =~ s/\.//g ;
    }
    if ( $line =~ /Add new (\w*) key[^(]*\(([^()]*)\)/) {
        my $type = $1;
        $type = "DD" if ($type eq "developer");
        my $name = $2;
        print "$date\t$type\t$name\n" if (defined($previous_dpn) and ($date >= $previous_dpn ));
    }

}

close (FILE);

# embedded documentation

__END__

=head1 NAME

dd-dm-from-keyring.pl - show DD and DM from keyring

=head1 SYNOPSIS

dd-dm-from-keyring.pl F<ISSUE>

=head1 OPTIONS

=over 8

=item B<--help>

Print a brief help message and exits.

=back

=head1 DESCRIPTION

This programm show DD and DM from keyring since specified DPN issue.

=head1 BUGS

Script don't care about DPN issue number i.e. always show list of DM and DD from year 2008.

=cut
