#! /bin/sh

# Author:	2011-2012 David Prévot <taffit@debian.org>
# License:	GPLv2 or later or, at your option, MIT (EXPAT)

# Usage: ./get-new.sh YYYYMMDD
# will feed the “list” file with new packages, since YYYYMMDD

arch=amd64
date=$1

# Run some tests

if [ "$date" = '' ]; then
	echo 'Usage: /get-new.sh YYYYMMDD'
	exit 1
fi

if [ -e "Packages.xz" ]; then
	echo "Error: Packages.bz2 already exists"
	exit 1
fi

if [ -e "Packages" ]; then
	echo "Error: Packages already exists"
	exit 1
fi

if [ -e "new" ]; then
	echo "Error: new already exists"
	exit 1
fi

if [ -e "old" ]; then
	echo "Error: old already exists"
	exit 1
fi

if [ -e "all" ]; then
	echo "Error: all already exists"
	exit 1
fi

if [ -e "both" ]; then
	echo "Error: both already exists"
	exit 1
fi

if [ -e "both" ]; then
	echo "Error: both already exists"
	exit 1
fi
# ...

# Download the old Package file
wget http://snapshot.debian.org/archive/debian/${date}/dists/sid/main/binary-${arch}/Packages.xz
xz -d Packages.xz
sort-dctrl -kPackage Packages > old
sort-dctrl -kPackage /var/lib/apt/lists/*_debian_dists_unstable_main_binary-${arch}_Packages > new

join-dctrl -j Package -a1 -o 0 new old > all
join-dctrl -j Package -o 0 new old > both

diff both all | grep '> Package: ' | sed -e 's,> Package: ,,' > list
